package com.example.baytik

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView

class Searchdetails : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        var btnFilter: ImageView? = null
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_searchdetails)
        btnFilter = findViewById(R.id.btnFilter)
        btnFilter.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this, Morefilters::class.java))

        })
    }
}