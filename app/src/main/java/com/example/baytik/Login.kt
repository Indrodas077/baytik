package com.example.baytik

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton

class Login : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {

        var btnRegister: TextView? = null
        var btnSignin: AppCompatButton? = null
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)
        btnRegister = findViewById(R.id.btnRegister)
        btnSignin = findViewById(R.id.btnSignin)

        btnRegister.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this@Login, Registration::class.java))
        })

        btnSignin.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this@Login, MainActivity ::class.java))
        })

    }
}