package com.example.baytik

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.widget.AppCompatButton

class OTP : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        var btnRegister: AppCompatButton ? =null
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_otp)
        btnRegister = findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this, MainActivity::class.java))

        })
    }
}