package com.example.baytik

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatButton
import androidx.cardview.widget.CardView

class Searchresults : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        var btnModify: AppCompatButton? = null
        var btnFilter: ImageView? = null
        var propertydetails : CardView? = null
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_searchresults)
        btnModify = findViewById(R.id.btnModify)
        btnFilter = findViewById(R.id.btnFilter)
        propertydetails = findViewById(R.id.propertydetails)
        btnModify.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this, Searchdetails::class.java))
        })
        btnFilter.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this, Morefilters::class.java))

        })

        propertydetails.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this, Propertydetails::class.java))

        })
    }
}