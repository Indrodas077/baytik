package com.example.baytik

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatButton

class Search : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        var btnSearch: AppCompatButton ?=null
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)
        btnSearch = findViewById(R.id.btnSearch)
        btnSearch.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this, Searchresults::class.java))
        })
    }
}