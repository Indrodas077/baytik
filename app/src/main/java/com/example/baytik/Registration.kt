package com.example.baytik

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import androidx.appcompat.widget.AppCompatButton

class Registration : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        var btnRegister: AppCompatButton? = null
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        btnRegister = findViewById(R.id.btnRegister)
        btnRegister.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this, OTP::class.java))
        })
    }
}