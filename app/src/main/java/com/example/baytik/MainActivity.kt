package com.example.baytik

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.view.View
import android.widget.TextView
import androidx.appcompat.widget.LinearLayoutCompat

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        var btnSearch: LinearLayoutCompat? = null
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnSearch = findViewById(R.id.btnSearch)

        btnSearch.setOnClickListener(View.OnClickListener {
            startActivity(Intent(this, Search::class.java))
        })



    }
}